# Sphinx4 Proof-of-concept

This repository contains a proof-of-concept code for speach recognition using the Sphinx4 library.

Test wave file results in the following string:

> thank you for choosing be unimpressed dictation management system the event a steak they should management system gives you the power to manage or dictation nice transcriptions and documents seamlessly and to improve the productivity of your baby what for example you could automatically sends the dictation files so transcribe documents to your system to feed also by email all f t p if you're using the speech recognition software the speech recognition engine the what's in the background to support joe document creation we hope you enjoy the simple flexible reliable and secure solutions from an impetus

Please note that the audio for this decoding must have one of the following formats:

```RIFF (little-endian) data, WAVE audio, Microsoft PCM, 16 bit, mono 16000 Hz```

or

```RIFF (little-endian) data, WAVE audio, Microsoft PCM, 16 bit, mono 8000 Hz```

Also, do checkout the [tutorial](https://cmusphinx.github.io/wiki/tutorialsphinx4/).

## Models

Various language models can be downloaded [here](https://sourceforge.net/projects/cmusphinx/files/Acoustic%20and%20Language%20Models/)